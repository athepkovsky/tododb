// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD6B-R0xCiKOq76xCbYOcT9RKXDH6bZK48',
    authDomain: 'todo-44539.firebaseapp.com',
    databaseURL: 'https://todo-44539.firebaseio.com',
    projectId: 'todo-44539',
    storageBucket: 'todo-44539.appspot.com',
    messagingSenderId: '504116835646'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
