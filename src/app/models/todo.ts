export class Todo {
  label: string;
  description: string;
  order: number;
  deadline: Date;
  done: boolean;
  categories: Array<string>;
}
