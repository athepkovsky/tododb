import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TodoComponent} from './todo/todo.component';
import {MoviesComponent} from './movies/movies.component';

const routes: Routes = [
  {path: 'todo', component: TodoComponent},
  {path: 'movies', component: MoviesComponent},
  {path: '', redirectTo: '/todo', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
