import {Component} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  navLinks = [
    {path: 'todo', label: 'ToDo'},
    {path: 'movies', label: 'Фильмы'}
  ];

  constructor() {}
}
