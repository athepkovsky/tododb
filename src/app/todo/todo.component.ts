import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

import { Todo } from '../models/todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  todos: Observable<Todo[]>;

  constructor(
    private db: AngularFirestore,
  ) {
    this.todos = <Observable<Todo[]>>db.collection('todos',
        ref => ref.where('done', '==', false).orderBy('order'))
      .valueChanges();
  }

  ngOnInit() {
  }

}
